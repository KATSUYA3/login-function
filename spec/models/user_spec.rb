require 'rails_helper'
require 'securerandom'

RSpec.describe User, type: :model do

  describe "#self.from_omniauth" do
    let(:auth){ 
      { 
        "provider" => "test_provider",
        "uid" => "test_uid",
        "info" => { "nickname" => "testuser" }
      } 
    }
    context "when user is not created" do
      it "succeeded in creating user." do
        expect{ User.from_omniauth(auth) }.to change{ User.count }.by(1)
      end
    end
    context "when user has already been created" do
      it "succeeded in creating user." do
        User.from_omniauth(auth)
        expect{ User.from_omniauth(auth) }.not_to change{ User.count }
      end
    end
  end

end

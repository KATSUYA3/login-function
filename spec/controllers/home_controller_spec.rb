require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    context "when login whith admin" do
      it "returns http success" do
        login_with_admin 
        get :show
        expect(response).to have_http_status(:success)
      end
    end
  end

end

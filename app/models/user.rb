require 'securerandom'

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :timeoutable, :omniauthable, omniauth_providers: [:twitter]

  # レスポンスの環境変数からprovider,uidを取得し、ユーザのレコードに追加する
  # auth: oauth認証から送られてきた認証情報
  def self.from_omniauth(auth)
    user = find_or_create_by!(provider: auth["provider"], uid: auth["uid"]) do |user|
      user.skip_confirmation!
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.user_name = auth["info"]["nickname"]
      user.email = auth["email"].nil? ? auth["provider"] + auth["uid"] + "@login.function" : auth["email"]
      user.password = SecureRandom.urlsafe_base64(16)
    end
  end

end

class HomeController < ApplicationController
  before_action :required_sign_in, only: [:show]

  def index
  end

  def show
  end
end

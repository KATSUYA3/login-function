class ApplicationController < ActionController::Base
  def after_sign_in_path_for(resource)
    show_path
  end

  private
  def required_sign_in
    redirect_to root_path unless user_signed_in?
  end
end
